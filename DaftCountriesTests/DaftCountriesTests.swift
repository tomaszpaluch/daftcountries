//
//  DaftCountriesTests.swift
//  DaftCountriesTests
//
//  Created by tomaszpaluch on 03.05.2019.
//  Copyright © 2019 tomaszpaluch. All rights reserved.
//

import XCTest
@testable import DaftCountries

class DaftCountriesTests_RestServiceBasicFunctionality: XCTestCase {
    var restService: RestService!
    
    override func setUp() {
        restService = RestService(reloadAction: {})
        
        let bundle = Bundle(for: type(of: self))
        let url = bundle.url(forResource: "all", withExtension: "json")
        
        restService.TEST_getAllCountries(url: url!)
    }

    func testGetLastInquiryCount_InquiryOK_Returns250() {
        XCTAssertEqual(restService.getLastInquiryCount(), 250)
    }
    
    func testGetCountryNameAtSpecifiedIndex_Index178_ReturnsPoland() {
        XCTAssertEqual(restService.getNameAt(index: 178), "Poland")
    }
}

class DaftCountriesTests_JSONSerialization: XCTestCase {
    func testJSONSerialization_EveryCountry_NoThrow() {
        let restService = RestService() {}
        
        let bundle = Bundle(for: type(of: self))
        let url = bundle.url(forResource: "all", withExtension: "json")
        
        XCTAssertNoThrow({restService.TEST_getAllCountries(url: url!)})
    }
    
    func testJSONSerialization_Poland_NoThrow() {
        let restService = RestService() {}
        
        let bundle = Bundle(for: type(of: self))
        let url = bundle.url(forResource: "Poland", withExtension: "json")
        
        XCTAssertNoThrow({restService.TEST_getCountryBy(url: url!)})
    }
    
    func testJSONSerialization_404Error_NoThrow() {
        let restService = RestService() {}
        
        let bundle = Bundle(for: type(of: self))
        let url = bundle.url(forResource: "404", withExtension: "json")
        
        XCTAssertNoThrow({restService.TEST_getCountryBy(url: url!)})
    }
}

class DaftCountriesTests_RestServiceGetCountry: XCTestCase {
    var restService: RestService!
    var url: URL!
    
    override func setUp() {
        restService = RestService(reloadAction: {})
        let bundle = Bundle(for: type(of: self))
        url = bundle.url(forResource: "Poland", withExtension: "json")!
    }
    
    func testJSONSerialization_Poland_StructsEqual() {
        let fakeStruct = RestCountryFullInfo(name: "Poland", capital: "Warsaw", altSpellings: ["PL","Republic of Poland","Rzeczpospolita Polska"], population: 38437239, latlng: [52.0,20.0], area: 312679.0, flag: URL(string: "https://restcountries.eu/data/pol.svg")!)
        
        let structFromJSON = restService.TEST_getCountryBy(url: url)
        
        XCTAssertEqual(structFromJSON, fakeStruct)
    }
}

extension RestCountryFullInfo: Equatable {
    public static func == (lhs: RestCountryFullInfo, rhs: RestCountryFullInfo) -> Bool {
        return lhs.name == rhs.name &&
            lhs.capital == rhs.capital &&
            lhs.altSpellings.sorted() == rhs.altSpellings.sorted() &&
            lhs.population == rhs.population &&
            lhs.latlng.sorted() == rhs.latlng.sorted() &&
            lhs.area == rhs.area &&
            lhs.flag == rhs.flag
    }
}
