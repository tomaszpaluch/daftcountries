//
//  ViewController.swift
//  DaftCountries
//
//  Created by tomaszpaluch on 03.05.2019.
//  Copyright © 2019 tomaszpaluch. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    private var restService: RestService!
    private var searchTimer: Timer?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        restService = RestService() { [unowned self] in self.tableView.reloadData() }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
        searchBar.delegate = self
        
        restService.getAllCountries()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? CountryDetailsViewController {
            let selectedRow = tableView.indexPathForSelectedRow!.row
           
            restService.getDetailedInformationsOfACountryAt(index: selectedRow) { [unowned destinationVC] (restCountryFullInfo: RestCountryFullInfo) -> Void in
                    destinationVC.setView(countryInformations: restCountryFullInfo)
            }
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return restService.getLastInquiryCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath) as! TableCell
        
        let countryName = restService.getNameAt(index: indexPath.row)
        cell.setCountryName(name: countryName)
        
        return cell
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchTimer != nil {
            searchTimer!.invalidate()
        }
        searchTimer = Timer.scheduledTimer(
            timeInterval: 1,
            target: self,
            selector: #selector(searchForCountries(sender:)),
            userInfo: ["tag": searchText],
            repeats: false
        )
    }
    
    @objc private func searchForCountries(sender: Timer) {
        if let info = sender.userInfo as? [String: String] {
            let phrase = info["tag"]!
            
            if phrase.count > 0 {
                restService.getCountriesBy(phrase: phrase)
            } else {
                restService.getAllCountries()
            }
        }
    }
}

