//
//  TableCellTableViewCell.swift
//  DaftCountries
//
//  Created by tomaszpaluch on 03.05.2019.
//  Copyright © 2019 tomaszpaluch. All rights reserved.
//

import UIKit

class TableCell: UITableViewCell {
    @IBOutlet private weak var countryNameLabel: UILabel!
    
    func setCountryName(name: String) {
        countryNameLabel.text = name;
    }
}
