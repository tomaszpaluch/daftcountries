//
//  CountryDetailsViewController.swift
//  DaftCountries
//
//  Created by tomaszpaluch on 04.05.2019.
//  Copyright © 2019 tomaszpaluch. All rights reserved.
//

import UIKit
import MapKit
import SVGKit

class CountryDetailsViewController: UIViewController {
    @IBOutlet private weak var mainNameLabel: UILabel!
    @IBOutlet private weak var secondaryNameLabel: UILabel!
    @IBOutlet private weak var capitalNameLabel: UILabel!
    @IBOutlet private weak var populationCountLabel: UILabel!
    @IBOutlet private weak var areaSizeLabel: UILabel!
    
    @IBOutlet private weak var flagImage: UIImageView!
    @IBOutlet private weak var mapView: MKMapView!
    
    private var semaphore: DispatchSemaphore
    
    required init?(coder aDecoder: NSCoder) {
        semaphore = DispatchSemaphore(value: 0)
        
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        prepareView()
    }
    
    private func prepareView() {
        mainNameLabel.text = "..."
        secondaryNameLabel.text = "..."
        capitalNameLabel.text = "..."
        populationCountLabel.text = "..."
        areaSizeLabel.text = "..."
        
        flagImage.backgroundColor = .gray
        flagImage.layer.masksToBounds = true
        flagImage.layer.borderWidth = 0.5
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        semaphore.signal()
    }
    
    func setView(countryInformations: RestCountryFullInfo) {
        semaphore.wait()
        
        setLabels(countryInformations: countryInformations)
        setMap(countryInformations: countryInformations)
        
        if let flag = prepareFlag(flagURL: countryInformations.flag) {
            setFlag(flagImage: flag)
        }
    }
    
    private func setLabels(countryInformations: RestCountryFullInfo) {
        DispatchQueue.main.async {
            self.mainNameLabel.text = countryInformations.name
            self.secondaryNameLabel.text = countryInformations.altSpellings.last
            self.capitalNameLabel.text = countryInformations.capital
            self.populationCountLabel.text = String(countryInformations.population)
            self.areaSizeLabel.text = String(format: "%0.0f km²", countryInformations.area)
        }
    }
    
    private func setMap(countryInformations: RestCountryFullInfo) {
        let distance = aproximateDistance(area: countryInformations.area)
        let clLocationDistance = CLLocationDistance(distance)
        let latitude = countryInformations.latlng[0]
        let longitude = countryInformations.latlng[1]
        
        let coordinate = CLLocationCoordinate2DMake(CLLocationDegrees(exactly: latitude)!, CLLocationDegrees(exactly: longitude)!)
        
        let camera = MKMapCamera(lookingAtCenter: coordinate, fromDistance: clLocationDistance, pitch: 0, heading: 0)
        
        DispatchQueue.main.async {
            self.mapView.setCenter(coordinate, animated: false)
            self.mapView.setCamera(camera, animated: true)
        }
    }
    
    private func aproximateDistance(area: Float) -> Float {
        return 3.5 * sqrt(1.8 * area) * 1000
    }
    
    private func prepareFlag(flagURL: URL) -> UIImage? {
        var svgFlag: SVGKImage? = nil
        
        do {
            let data = try Data(contentsOf: flagURL)
            svgFlag = SVGKImage(data: data)
        }
        catch {
            print("image processor error")
        }
        
        return svgFlag?.uiImage
    }
    
    private func setFlag(flagImage: UIImage) {
        DispatchQueue.main.async {
            self.flagImage.image = flagImage
        }
    }
}
