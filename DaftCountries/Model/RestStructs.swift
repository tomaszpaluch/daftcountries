//
//  RestStructs.swift
//  DaftCountries
//
//  Created by tomaszpaluch on 11.05.2019.
//  Copyright © 2019 tomaszpaluch. All rights reserved.
//

import Foundation

protocol RestCountryProtocol: Decodable {
    var name: String { get set }
}

struct RestCountryBaseInfo: RestCountryProtocol {
    var name: String
}

struct RestCountryFullInfo: RestCountryProtocol {
    var name: String
    var capital: String
    var altSpellings: [String]
    var population: Int
    var latlng: [Float]
    var area: Float
    var flag: URL
}
