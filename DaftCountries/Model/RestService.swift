//
//  File.swift
//  DaftCountries
//
//  Created by tomaszpaluch on 03.05.2019.
//  Copyright © 2019 tomaszpaluch. All rights reserved.
//

import Foundation

class RestService {
    private var lastInquiry : [String]
    private let url: URL
    private var reloadAction: (() -> Void)
    
    private var serviceURL = "https://restcountries.eu/rest/v2/"
    
    init(reloadAction: @escaping (() -> Void)) {
        lastInquiry = [String]()
        url = URL(string: serviceURL)!
        
        self.reloadAction = reloadAction
    }
    
    func getAllCountries() {
        let pathComponent = "all"
        getCountriesBy(pathComponent: pathComponent)
    }
    
    func getCountriesBy(phrase: String) {
        let pathComponent = "name/" + phrase + "/"
        getCountriesBy(pathComponent: pathComponent)
    }
    
    func getLastInquiryCount() -> Int {
        return lastInquiry.count;
    }
    
    func getNameAt(index: Int) -> String {
        return lastInquiry[index];
    }
    
    func getCountriesBy(pathComponent: String) {
        let actualUrl = createActualURL(pathComponent: pathComponent, queryItems: [URLQueryItem(name: "fields", value: "name")])
        
        lastInquiry.removeAll()
        
        makeInquiry(url: actualUrl) { (responseModel: [RestCountryBaseInfo]) -> Void in
            responseModel.forEach({self.lastInquiry.append($0.name)})
            DispatchQueue.main.async {
                self.reloadAction()
            }
        }
    }
    
    func getDetailedInformationsOfACountryAt(index: Int, completion: @escaping (RestCountryFullInfo) -> Void) {
        let name = getNameAt(index: index)
        let pathComponent = "name/" + name + "/"
        let actualUrl = createActualURL(
            pathComponent: pathComponent,
            queryItems: [URLQueryItem(name: "fields", value: "name;capital;altSpellings;population;latlng;area;flag")]
        )

        makeInquiry(url: actualUrl) { (responseModel: [RestCountryFullInfo]) -> Void in
            let detailedInfo = responseModel.first!
            completion(detailedInfo)
        }
    }
    
    private func createActualURL(pathComponent: String, queryItems: [URLQueryItem]) -> URL {
        let tempUrl = url.appendingPathComponent(pathComponent)
        var urlComponents = URLComponents(string: tempUrl.absoluteString)!
        urlComponents.queryItems = queryItems
        
        return urlComponents.url!
    }
    
    private func makeInquiry<T: RestCountryProtocol>(url: URL, completion: @escaping ([T]) -> Void) {
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request) { data, response, error -> Void in
            do {
                let jsonDecoder = JSONDecoder()
                let responseModel = try jsonDecoder.decode([T].self, from: data!)
                completion(responseModel)
            } catch {
                print("JSON Serialization error \n\(error)")
            }
        }.resume()
    }
    
    
    func TEST_getAllCountries(url: URL) {
        var readyToGo = false
        
        makeInquiry(url: url, completion: { (responseModel: [RestCountryBaseInfo]) -> Void in
            responseModel.forEach({self.lastInquiry.append($0.name)})
            readyToGo = true
        })
        
        while(!readyToGo) {}
    }
    
    func TEST_getCountryBy(url: URL) -> RestCountryFullInfo {
        var restCountryFullInfo: RestCountryFullInfo? = nil
        var readyToGo = false
        
        makeInquiry(url: url, completion: {(responseModel : [RestCountryFullInfo]) -> Void in
            restCountryFullInfo = responseModel.first!
            readyToGo = true
        })
        
        while !readyToGo {}
        return restCountryFullInfo!
    }
}
